from application import app
from flask import render_template


@app.route('/blog', methods=['OPTIONS', 'GET', 'POST'])
def blog():
    """Return a friendly HTTP greeting."""
    return render_template('base.htm')


@app.route('/blog/<int:blog_id>', methods=['OPTIONS', 'GET', 'PUT', 'DELETE'])
def blog_post_by_id(blog_id):
    """Return a blog post for the give blog id"""
    return 'Blog id : %d' % (blog_id)


@app.route('/blog/<slug>', methods=['OPTIONS', 'GET', 'PUT', 'DELETE'])
def blog_post_by_slug(slug):
    """Return a blog post for the given slug"""
    return 'Slug is %s' % slug
